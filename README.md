# BVI Safety Check

> A PHP and Vue.js project
Website is available at [BVI Safety Check](http://www.bvisafetycheck.com/)

This website was created to act as a single source of truth for those affected by the devastation of Hurricane Irma.

Many groups were hastily creating spreadsheets to keep track of people that were missing or not heard from. The hurricane destroyed all phone and internet across the BVI which made it impossible for the people there to check in with their loved ones.
I created this website the night after the storm, finishing work at 6PM and then having the V1 online that night.

This website still requires alot of work if it was to be maintained (primarily the PHP side of things and the DB statements).

I wrote custom import scripts for the maintained spreadsheets and imported these records into the database so that all records were up to date automatically for the groups.

After doing this I steadily added features each night (such as the ordering of the table and the pagination).

The frontend on this project is pretty solid, and given the time spent on it I am very proud of the outcome, I normally like making use of VueX and more modular components but given that this was a time critical project I went without and it is incredibly
stable given the way that it serves mass data.



## Build Setup

``` bash
# install dependencies
npm install

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
