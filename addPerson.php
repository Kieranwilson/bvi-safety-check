<?php

require_once('inc.php');

$payload = file_get_contents('php://input');

$data = json_decode($payload);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	header('X-PHP-Response-Code: 500', true, 500);
}

/***
stdClass Object
(
    [persons] => Array
        (
            [0] => stdClass Object
                (
                    [fName] => Kieran
                    [lName] => Wilson
                    [location] => Island 1
                )

            [1] => stdClass Object
                (
                    [fName] => Kieran
                    [lName] => Wilson
                    [location] => Island 2
                )

        )

    [submitter] => stdClass Object
        (
            [fname] => Kieran
            [lname] => Wilson
            [email] => kieran@leafcutter.com.au
            [contactInfo] => Call me on 123456789
        )

)
***/
$submitter = $data->submitter->fname . ' ' . $data->submitter->lname;
$semail = $data->submitter->email;
$contactinfo = $data->submitter->contactInfo;

foreach ($data->persons as $person) {
	$fname = $person->fName;
	$lname = $person->lName;
	$location = $person->location;
	$nationality = $person->nationality;
	$status = $person->status;

	$sql = "INSERT INTO $table (submitter, submitter_email, person_fname, person_lname, location, contactinfo, nationality, status)
VALUES ('$submitter', '$semail', '$fname', '$lname', '$location', '$contactinfo', '$nationality', $status)";

	if ($conn->query($sql) === TRUE) {
		//echo "New record created successfully";
	} else {
		header('X-PHP-Response-Code: 500', true, 500);
	}
}
$conn->close();


?>