<?php

require_once('inc.php');

$payload = file_get_contents('php://input');

$data = json_decode($payload);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	header('X-PHP-Response-Code: 500', true, 500);
	die("Connection failed: " . $conn->connect_error);
}

$status = $data->status;
$id = $data->id;

$sql = "UPDATE $table SET status='$status' WHERE id=$id";

if ($conn->query($sql) === TRUE) {
    echo "Record updated successfully";
} else {
    echo "Error updating record: " . $conn->error;
}

$sql = "SELECT * FROM $table WHERE id=$id";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {

		if ($status == 1) {
			$msg = "Dear " . $row['submitter_fname'] . " " . $row['submitter_lname'] . ",

The following person has been marked as safe on http://www.bvisafetycheck.com/.

" . $row['person_fname'] . " " . $row['person_lname'] . "

Regards,

The BVI Safe team";

			// send email
			mail($row['submitter_email'],"BVI Safety Check Update",$msg);
		}
	}
}
$conn->close();

?>
