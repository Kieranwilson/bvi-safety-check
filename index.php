<?php

require_once('inc.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM $table";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
	// output data of each row
	$results = $result->fetch_all(MYSQLI_ASSOC);
}

$safe = $conn->query("SELECT * FROM $table WHERE `status` = 1")->num_rows;
$unkown = $conn->query("SELECT * FROM $table WHERE `status` = 0")->num_rows;

$conn->close();
?><!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" type="image/png" href="/favicon.png">
  <meta charset="utf-8">
  <title>BVI SafetyCheck - Hurricane Irma Safety Check</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106146308-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-106146308-1');
  </script>
</head>
<body>
  <!-- Global Site Tag (gtag.js) - Google Analytics -->
  <script type="text/javascript">
    window.People = <?php echo json_encode($results); ?>;
	window.Safe = <?php echo json_encode($safe); ?>;
	window.Unknown = <?php echo json_encode($unkown); ?>;
  </script>
  <div id="app"></div>
  <footer class="blog-footer pt-3 pb-3 text-center">
    <p>Site powered by <a href="https://leafcutter.com.au/">Leafcutter</a> &copy; 2017. <a href="/privacy-policy.php">Privacy Policy</a></p>
  </footer>
  <script src="./dist/build.js?v=1.06"></script>
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59b396d7b6783f4c"></script>
</body>
</html>
