<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" type="image/png" href="/favicon.png">
  <meta charset="utf-8">
  <title>BVI SafetyCheck - Hurricane Irma Safety Check</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106146308-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-106146308-1');
  </script>
</head>
<body>
<style lang="scss">
  html {
    font-size: 14px;
  }
  .col-auto, .col-sm-auto { padding-left: 15px; padding-right: 15px; }
  .form-group { margin: 0; }
  #app {
    font-family: 'Avenir', Helvetica, Arial, sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-align: center;
    color: #2c3e50;
    margin-top: 60px;
  }

  .intro-wrapper {
    background: #EEEEEE;
    border-top: 2px solid black;
    border-bottom: 2px solid black;
  }
  .removePerson {
    cursor: pointer;
    position: absolute;
    right: 5px;
    color: red !important;
    top: 7px;
    z-index: 9;
  }
  .row { position: relative; }
  .head-wrapper img {
    max-width: 100%;
    max-height: 68px;
  }
  .reopen { text-decoration: underline !important; color: #428dcc !important; }
  th.location, td.location {
    max-width: 300px;
  }
  td.sname, th.sname, th.pname, td.pname {
    max-width: 150px;
  }
  th.status, td.status {
    max-width: 190px;
  }
</style>
  <div class="head-wrapper">
      <div class="container">
        <div class="row align-items-center justify-content-between mt-2 mb-2">
          <div class="col-8 col-sm-auto">
          <a href="/">
            <img src="/img/logo.png" />
            </a>
          </div>
        </div>
      </div>
    </div>
  <div class="intro-wrapper">
      <div class="container">
        <div class="row mt-4 mb-4">
          <div class="col-12">
            <h1 class="mb-4">Privacy Policy</h1>
            <p>Information that is gathered from visitors</p>

<p>In common with other websites, log files are stored on the web server saving details such as the visitor's IP address, browser type, referring page and time of visit.</p>

<p>Cookies may be used to remember visitor preferences when interacting with the website.</p>

<p>Where registration is required, the visitor's email and a username will be stored on the server.</p>

<p>How the Information is used</p>

<p>The information is used to enhance the vistor's experience when using the website to display personalised content and possibly advertising.</p>

<p>E-mail addresses will not be sold, rented or leased to 3rd parties.</p>

<p>E-mail may be sent to inform you of news of our services or offers by us or our affiliates.</p>
          </div>
        </div>
      </div>
    </div>
  <footer class="blog-footer pt-3 pb-3 text-center">
    <p>Site powered by <a href="https://leafcutter.com.au/">Leafcutter</a> &copy; 2017. <a href="/privacy-policy.php">Privacy Policy</a></p>
  </footer>
</body>
</html>
